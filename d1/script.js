let grades = [98.5, 95.3, 89.2, 90.1]

console.log(grades[0])

let grade = {
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
}
// we used dot notatation to access the properties/value of our object
console.log(grade.english)

// Syntax:

/*
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/
let cellphone =  {
	brandName: 'Nokia 3310',
	color:  'Dark Blue',
	manufactureDate: 1999
}

	console.log(typeof cellphone)

let student = {
	firstName: 'John',
	lastName: 'Smith',
	mobileNo: '0912 234 5678',
	location: {
		city: 'Tokyo',
		country: 'Japan'
	},
	emails: ['john@mail.com', 'johnsmith@mail.xyz'],

	// onject method(function inside an object)
	fullName: function(){
		return this.firstname + '' + this.lastName
	}
}

console.log(student.location.city)
// bracket notation
console.log(student['firstName'])

console.log(student.emails)
console.log(student.emails[0])

console.log(student.fullName())


const person1 = {
	name: 'Jane',
	greeting: function(){
		return 'Hi I\'m' + this.name
	}
}

console.log(person1.greeting)

// ARRAY OF OBJECTS
let contactList = [
	{
		firstName: "John",
		lastName: "Smith",
		location: "Japan"
	},
	{
		firstName: "Jane",
		lastName: "Smith",
		location: "Japan"
	},
	{
		firstName: "Jasmine",
		lastName: "Smith",
		location: "Japan"
	}
]
console.log(contactList[0].firstName)


let people = [

	{
		name: "Juanita",
		age: 13
	},
	{
		name: "Juanito",
		age: 14
	},
]
people.forEach(function(person){
	console.log(person.name)
})


console.log(`${people[0].name} are the list`)

//  Creating onjects using a contructor Function( JS OBJACT CONTRUCTOR/OBJECT FROM BLUEPRINT)

// Create a reusable function to create several objects that have the same data structure
// This is usefull for creating multiple copies/instances of an object
// Object Literals
	// let object = {}
// Instance - distinctunique objects
	// let object = new object
// An instance is a concrete occurence of any object which emphasize on the uni identity of it
/*
	Syntax:
		function Objectname(keyA, keyB){
			this.keyA = keyA,
			this.keyB = keyB
		}

*/
function Laptop(name,manufactureDate){
	// the 'this' keyword allow to assign a new object's property by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is a unique instance of the laptop object

let laptop = new Laptop('Lenovo', 2008)
console.log("Result from creating objects using object constructors")
console.log(laptop)

// This is another unique instance of the laptop object

let myLaptop = new Laptop("MacBook Air", 2020)
console.log("Result from creating objects using object constructors")
console.log(myLaptop)
let oldLaptop = Laptop("Portal R2E CCMC", 1980)
console.log("without new keyword")
console.log(oldLaptop)

// Creating empty objects
let computer = {}
let myComputer = new Object();

console.log(myLaptop.name)

let array = [laptop, myLaptop]

console.log(array[0].name)

// Initializing/Adding/Deleting/Reassigning Object Properties

// initialized/added properties after the object was created
// This is usefull for times when object's properties are undertermined at the time of creating them

let car = {}

// add an object properties, we can use dot notation

car.name = "Honda Civic"
console.log(car)

car["manufactureDate"] = 2019;
console.log(car)

// Reassigning object properties
car.name = "Dodge Charger R/T"
console.log(car)

// Deleting object properties
delete car["manufactureDate"]
console.log("Result from deleting properties")
console.log(car)

// OBJECT METHOD
// A method is a function which is a property of an object
// They are also functions and one of the key differences they have is that methods are functions related to a specific onject

let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is " + this.name)
	}
}
console.log(person)
console.log("Result from object ,ethods")
person.talk()

// Adding method to object person

person.walk = function(){
	console.log(this.name + " walked 25 steps forward")
}
person.walk()

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com", "joesmith@mail.xyz"],
	introdunce: function(){
		console.log("Hello my name is " + this.firstname + this.lastName + "i live in " + this.address.city)
	}
}
friend.introdunce()


let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokeon tackled target Pokemon")
		console.log("'targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// 

	this.tackle = function(target){
		console.log(this.name + " tackled" + target
			.name)
		console.log("tagrgetPokemon's health is now reduced to _targetPokemonHealth_")
	};
	this.faint =  function(){
		console.log(this.name = 'fainted.')
	}
}
let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)

pikachu.tackle(charizard)