let trainer = {
	name: "Ash ketchu",
	age: 10,
	pokemon: ["Pikachu", "Charizord", "Squirtle", "Bulbassaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	}
}
console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name)
console.log("Result of square bracket notation:")
console.log(trainer["pokemon"])

let trainer1 = {
	name: "Ash",
	talk: function(){
		console.log(`Pikachu I choose you!`)
	}
}
console.log("Result of talk method")
trainer1.talk()

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2.5 * level;
	this.attack = 1.5 * level;

	// 

	this.tackle = function(target){

	console.log(this.name + " tackled" + target
			.name)
	console.log(`${target.name} health is now reduced to ${this.attack - target.health}`)
	if(target.health >= 0){
		console.log("Fainted")
	}

	};

}
let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)
let geodude = new Pokemon ('Geodude', 10)
pikachu.tackle(charizard)
geodude.tackle(charizard)